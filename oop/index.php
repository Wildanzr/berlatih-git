<?php
require_once ("./animal.php");
require ("./Ape.php");
require ("./Frog.php");

$sheep = new Animal("shaun");
$sungokong = new Ape("Kera sakti");
$frog = new Frog("Buduk");

echo ("Name: " . $sheep->name . "<br>");
echo ("Legs: " . $sheep->legs . "<br>");
echo ("Cold-blooded: " . $sheep->cold_blooded . "<br><br>");

echo ("Name: " . $frog->name . "<br>");
echo ("Legs: " . $frog->legs . "<br>");
echo ("Cold-blooded: " . $frog->cold_blooded . "<br>");
echo ("Jump: " . $frog->jump() . "<br><br>");

echo ("Name: " . $sungokong->name . "<br>");
echo ("Legs: " . $sungokong->legs . "<br>");
echo ("Cold-blooded: " . $sungokong->cold_blooded . "<br>");
echo ("Yell: " . $sungokong->yell() . "<br><br>");